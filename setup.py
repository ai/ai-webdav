#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name="ai-webdav",
    version="0.2",
    description="WebDAV serving stack",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai/ai-webdav",
    install_requires=["python-ldap", "python-pam", "wsgidav>=3", "flup"],
    setup_requires=[],
    packages=find_packages(),
    package_data={},
    entry_points={
        'console_scripts': [
          'ai-webdav-auth-server = ai_webdav.auth_server:main',
          'dav.fcgi = ai_webdav.dav_server:main',
          ],
        }
    )

