
*OBSOLETE*

This project has been replaced by

* [webdav-server](https://git.autistici.org/ai3/tools/webdav-server) for the FastCGI server side
* [webdav-auth](https://git.autistici.org/ai3/tools/webdav-auth) for the auth-server bridge

