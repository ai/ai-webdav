from __future__ import print_function

import ldap
import mock
import unittest
import urllib
import sys
from wsgi_intercept import requests_intercept, add_wsgi_intercept, remove_wsgi_intercept
try:
    from StringIO import StringIO
except ImportError:
    from io import BytesIO as StringIO

from ai_webdav import dav_server
from ai_webdav.test.test_auth_server import DavAuthServerTestBase
from ai_webdav.test import *
from ai_webdav.test import easywebdav

from nose.tools import nottest

DAV_HOST = 'example.com'
DAV_PORT = 80


def fix_env(app):
    # Oops, wsgidav is not happy if QUERY_STRING is not present at all
    # in the WSGI environment, so define it to an empty string if
    # necessary.
    def _fix_env(environ, start_response):
        if 'QUERY_STRING' not in environ:
            environ['QUERY_STRING'] = ''
        return app(environ, start_response)
    return _fix_env


class TestDavHandler(DavAuthServerTestBase):

    def setUp(self):
        super(TestDavHandler, self).setUp()

        # Create a test file to be accessed via WebDAV.
        self.dav_root = os.path.join(self.tmpdir, 'davroot')
        os.mkdir(self.dav_root)
        with open(os.path.join(self.dav_root, 'test.txt'), 'w') as fd:
            fd.write('testdata')

        # Make getpeercred() return the right user ID.
        self.gpc_patch = mock.patch(
            'ai_webdav.auth_server.getpeercred', return_value=(1, TEST_UID, 1))
        self.gpc_patch.start()

        # Mock the get_accounts() method so that it returns a
        # path that we control.
        self.ga_patch = mock.patch(
            'ai_webdav.client.DavAuthClient.get_accounts', return_value={
                u'/dav/test': {
                    'dn': TEST_DN,
                    'ftpname': 'testdav',
                    'home': self.dav_root,
                },
            })
        self.ga_patch.start()

        self.app = dav_server.create_app(dict(
            auth_socket=self.socketpath,
            verbose=0))

        # easywebdav uses the Requests package.
        requests_intercept.install()
        add_wsgi_intercept(DAV_HOST, DAV_PORT, lambda: fix_env(self.app))
        self.dav_addr = '%s:%d' % (DAV_HOST, DAV_PORT)

    def tearDown(self):
        remove_wsgi_intercept()
        requests_intercept.uninstall()
        self.gpc_patch.stop()
        self.ga_patch.stop()
        super(TestDavHandler, self).tearDown()

    @mock.patch('ai_webdav.auth_server.pamh.authenticate', return_value=True)
    def test_dav_ok(self, pamh):
        client = easywebdav.connect(
            DAV_HOST, path='dav/test',
            username=TEST_FTPNAME, password=TEST_PASS)
        results = client.ls()
        files_found = sorted([os.path.basename(x.name) for x in results])
        self.assertEquals(['', 'test.txt'], files_found)

        self.assertEquals(
            'testdata',
            self._download(client, 'test.txt'))

    @nottest
    def test_dav_utf8(self):
        print('encodings: default=%s fs=%s' % (sys.getdefaultencoding(), sys.getfilesystemencoding()))
        path = u'H\xe5llo'
        os.mkdir(os.path.join(self.dav_root, 'utf8'))
        with open(os.path.join(self.dav_root, 'utf8', path.encode('utf-8')), 'w') as fd:
            fd.write('testdata')

        client = easywebdav.connect(
            DAV_HOST, path='dav/test/utf8',
            username=TEST_FTPNAME, password=TEST_PASS)
        results = client.ls()
        files_found = sorted([os.path.basename(x.name) for x in results])
        self.assertEquals(['', urllib.quote(path.encode('utf-8'))], files_found)

        self.assertEquals(
            'testdata',
            self._download(client, path.encode('utf-8')))

    def _download(self, client, path):
        outbuf = StringIO()
        client.download(path, outbuf)
        outbuf.seek(0)
        return outbuf.read().decode('utf-8')
        
    @mock.patch('ai_webdav.auth_server.pamh.authenticate', return_value=False)
    def test_dav_fails_with_bad_password(self, ldapbind):
        # We use the 'right' username/password to ensure that the
        # authentication fails because we've mocked the right thing.
        client = easywebdav.connect(
            DAV_HOST, path='dav/test',
            username=TEST_FTPNAME, password=TEST_PASS)
        self.assertRaises(easywebdav.OperationFailed, client.ls)
